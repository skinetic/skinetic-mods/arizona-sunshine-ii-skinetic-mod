﻿/*
 * This file is part of Arizona Sunshine 2 Skinetic Mod.
 *
 * Arizona Sunshine Skinetic Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Arizona Sunshine Skinetic Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using HarmonyLib;
using Il2CppVertigo.AZS2;
using Il2CppVertigo.AZS2.Client;
using MelonLoader;
using Skinetic;
using System;
using System.Collections.Generic;
using UnityEngine;




[assembly: MelonOptionalDependencies("Client")]

namespace ArizonaSunshine_SkineticMod
{
    public class AS_SkineticMod : MelonMod
    {
        private static SkineticManager _skineticHandler;
        private static bool _lowLifeEffectPlaying = false;
        private static int _lowLifeEffectId = -1;
        private static Vector3 _playerPosition;
        private static Quaternion _playerRotation;
        private static ClientPlayerHealthModule _posInstance;
        private static List<int> _shootingID = new List<int>();

        #region Skinetic Handling
        public override void OnInitializeMelon()
        {
            LoggerInstance.Msg("Loading Skinetic");
            _skineticHandler = new SkineticManager();
        }

        public override void OnApplicationQuit()
        {
            LoggerInstance.Msg("OnApplicationQuit");
            _skineticHandler.Disconnect();
        }
        #endregion

        #region Health
        private static float GetRelativeHitAngle(Vector3 playerPosition, Vector3 hitPosition, Quaternion playerRotation)
        {
            // Calculate the direction vector from the player to the hit position.
            Vector3 targetDir = hitPosition - playerPosition;

            // Project the target direction onto the horizontal plane to ignore the Y (up/down) component.
            Vector3 flattenedTargetDir = Vector3.ProjectOnPlane(targetDir, Vector3.up);

            // Calculate the player's forward direction.
            Vector3 playerForward = Vector3.ProjectOnPlane(playerRotation * Vector3.forward, Vector3.up);

            // Calculate the signed angle between the player's forward direction and the target direction.
            float relativeAngle = Vector3.SignedAngle(playerForward, flattenedTargetDir, Vector3.up);

            // Return the calculated relative angle.
            return relativeAngle;
        }


        [HarmonyPatch(typeof(ZombieMeleeAttackView), "OnAttackImpact")]
        public class MeleeAttack
        {
            [HarmonyPrefix]
            public static void Prefix(ZombieMeleeAttackView __instance)
            {
                _playerPosition = _posInstance.transformModule.HeadPosition;
                _playerRotation = _posInstance.transformModule.ChestRotation;
                float distance = Vector3.Distance(__instance.transformModule.RootPosition, _playerPosition);
                float meleeRange = 3.6f;
                if (distance <= meleeRange)
                {
                    float angle = GetRelativeHitAngle(_playerPosition, __instance.transformModule.RootPosition, _playerRotation);
                    angle = angle % 360;
                    if (angle < 0)
                    {
                        angle += 360;
                    }
                    _skineticHandler.Play(Patterns.ZombieHit, SkineticManager.InitEffectProperties(2, 100, 1, 1, 0, 0, 0, 0, false, 0, -angle));
                }
            }
        }

        [HarmonyPatch(typeof(ZombieLungeAttackView), "OnAttackImpact")]
        public class LungeAttack
        {
            [HarmonyPrefix]
            public static void Prefix(ZombieLungeAttackView __instance)
            {
                _playerPosition = _posInstance.transformModule.HeadPosition;
                _playerRotation = _posInstance.transformModule.ChestRotation;
                float distance = Vector3.Distance(__instance.transformModule.RootPosition, _playerPosition);
                float meleeRange = 3.6f;
                if (distance <= meleeRange)
                {
                    float angle = GetRelativeHitAngle(_playerPosition, __instance.transformModule.RootPosition, _playerRotation);
                    angle = angle % 360;
                    if (angle < 0)
                    {
                        angle += 360;
                    }
                    _skineticHandler.Play(Patterns.ZombieHit, SkineticManager.InitEffectProperties(2, 100, 1, 1, 0, 0, 0, 0, false, 0, -angle));
                }

            }
        }

        [HarmonyPatch(typeof(ClientPlayerHealthModule), "ApplyDamage", new System.Type[] { typeof(uint), typeof(uint), typeof(int), typeof(int), typeof(Il2CppSystem.Nullable<Vector3>), typeof(bool) })]
        public class ApplyDamage
        {
            [HarmonyPrefix]
            public static void Prefix(ClientPlayerHealthModule __instance, int hitBoneIndex, Il2CppSystem.Nullable<Vector3> hitOrigin, bool isKilled)
            {
                try
                {
                    if (!__instance.identityModule.IsLocal)
                    {
                        return;
                    }
                    
                    if (isKilled)
                    {
                        _skineticHandler.Stop(_lowLifeEffectId, 0.2f);
                        _lowLifeEffectPlaying = false;
                        _skineticHandler.Play(Patterns.Dead, SkineticManager.InitEffectProperties(1));
                        return;
                    }
                    else
                    {
                        HandleLowHealth(__instance.health.HealthValue);
                    }
                }
                catch (System.Exception ex)
                {
                    MelonLogger.Error($"Error in ApplyDamage Prefix: {ex.Message}\n{ex.StackTrace}");
                }
            }
        }

        [HarmonyPatch(typeof(ClientPlayerHealthModule), "ApplyHeal", new System.Type[] { typeof(uint), typeof(int) })]
        public class ApplyHeal
        {
            [HarmonyPostfix]
            public static void Postfix(ClientPlayerHealthModule __instance, uint healerId, int healAmountPrecise)
            {
                if (!__instance.identityModule.IsLocal) return;
                HandleLowHealth(__instance.health.HealthValue);
                _skineticHandler.Play(Patterns.Eating, SkineticManager.InitEffectProperties(6));
            }
        }

        public static void HandleLowHealth(float health)
        {
            if (health < 35.0f && !_lowLifeEffectPlaying)
            {
                Melon<AS_SkineticMod>.Logger.Msg("START LowLife effect - " + health);
                _lowLifeEffectId = _skineticHandler.Play(Patterns.LowLife, SkineticManager.InitEffectProperties(7));
                _lowLifeEffectPlaying = true;
            }
            else if (health >= 35.0f && _lowLifeEffectPlaying)
            {
                Melon<AS_SkineticMod>.Logger.Msg("STOP LowLife effect - " + health);
                _skineticHandler.Stop(_lowLifeEffectId, 0.2f);
                _lowLifeEffectPlaying = false;
            }
        }
        #endregion

        #region equipement
        [HarmonyPatch(typeof(HolsterHandleSlotBehaviour), "HandleOnInteractableRemovedEvent")]
        public class HandleOnInteractableRemovedEvent
        {
            [HarmonyPostfix]
            public static void Postfix(
                HolsterHandleSlotBehaviour __instance,
                Il2CppVertigo.Interactables.InteractableSlot<Il2CppVertigo.Interactables.InteractableHandle> slot,
                  Il2CppVertigo.Interactables.InteractableHandle handle)
            {
                if (Il2CppVertigo.AZS2.Client.PawnUtils.IsLocalPawnSlot(handle.Slot))
                {
                    if (__instance.slotType.ToString() == "RightHand")
                    {
                        _skineticHandler.Play(Patterns.StoreItem, SkineticManager.InitEffectProperties(8, 100, 1, 1, 0, 0, 0, 0, false, 0, -25));
                    }
                    else
                    {
                        _skineticHandler.Play(Patterns.StoreItem, SkineticManager.InitEffectProperties(8, 100, 1, 1, 0, 0, 0, 0, false, 0, 25));
                    }

                }
            }
        }

        [HarmonyPatch(typeof(Inventory), "AddResource")]
        public class InsertElement
        {
            [HarmonyPostfix]
            public static void Postfix(Inventory __instance)
            {
                _skineticHandler.Play(Patterns.StoreAmmo, SkineticManager.InitEffectProperties(8, 90));
            }
        }

        [HarmonyPatch(typeof(Inventory), "ReduceResource")]
        public class RemoveElement
        {
            [HarmonyPostfix]
            public static void Postfix(Inventory __instance)
            {
                _skineticHandler.Play(Patterns.RetrieveAmmo, SkineticManager.InitEffectProperties(8, 85));
            }
        }
        #endregion

        #region Weapon Related interactions

        private static Patterns GetWeaponPattern(string weaponName)
        {
            switch (weaponName)
            {
                case string name when (name.Contains("Coach") || name.Contains("Mossberg")):
                    return Patterns.ShotGunRecoil;
                case string name when name.Contains("Flamethrower"):
                    return Patterns.FlameThrower;
                case string name when name.Contains("AK"):
                    return Patterns.RifleRecoil;
                case string name when name.Contains("Remington"):
                    return Patterns.SniperRecoil;
                default:
                    return Patterns.GunRecoil;
            }
        }

        [HarmonyPatch(typeof(ShootStrategy), "OnShotFired")]
        public class OnShotFired
        {
            [HarmonyPostfix]
            public static void Postfix(ShootStrategy __instance)
            {
                List<Il2CppVertigo.VRShooter.Hand> hands = new List<Il2CppVertigo.VRShooter.Hand>();
                if (!__instance.item.IsGrabbedLocally)
                {
                    return;
                }
                _playerPosition = __instance.item.ItemBehaviour.gameObject.transform.position;
                _playerRotation = __instance.item.ItemBehaviour.gameObject.transform.rotation;
                if (GetWeaponPattern(__instance.item.Name.Text.ToString()) == Patterns.FlameThrower)
                {
                    if (__instance.item.grabbedHands.Count > 1)
                    {
                        _shootingID.Add(_skineticHandler.Play(GetWeaponPattern(__instance.item.Name.Text.ToString()), SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, false, false, false, true))); ;
                    }
                    else
                    {
                        foreach (Il2CppVertigo.VRShooter.Hand hand in __instance.item.grabbedHands)
                        {
                            _shootingID.Add(_skineticHandler.Play(GetWeaponPattern(__instance.item.Name.Text.ToString()), SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, hand.IsLeftHand, false, false, false)));
                        }
                    }
                }
                else
                {
                    if (__instance.item.grabbedHands.Count > 1)
                    {
                        _skineticHandler.Play(GetWeaponPattern(__instance.item.Name.Text.ToString()), SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, false, false, false, true));
                    }
                    else
                    {
                        foreach (Il2CppVertigo.VRShooter.Hand hand in __instance.item.grabbedHands)
                        {
                            _skineticHandler.Play(GetWeaponPattern(__instance.item.Name.Text.ToString()), SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, hand.IsLeftHand, false, false, false));
                        }
                    }
                }
            }
        }

        [HarmonyPatch(typeof(ShootStrategy), "OnShootEmpty")]
        public class OnShootEmpty
        {
            [HarmonyPostfix]
            public static void Postfix(ShootStrategy __instance)
            {
                List<Il2CppVertigo.VRShooter.Hand> hands = new List<Il2CppVertigo.VRShooter.Hand>();

                if (!__instance.item.IsGrabbedLocally)
                {
                    return;
                }
                _playerPosition = __instance.item.ItemBehaviour.gameObject.transform.position;
                _playerRotation = __instance.item.ItemBehaviour.gameObject.transform.rotation;
                if (__instance.item.grabbedHands.Count > 1)
                {
                    _skineticHandler.Play(Patterns.Empty, SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, false, false, false, true));
                }
                else
                {
                    foreach (Il2CppVertigo.VRShooter.Hand hand in __instance.item.grabbedHands)
                    {
                        _skineticHandler.Play(Patterns.Empty, SkineticManager.InitEffectProperties(4, 100, 1, 1, 0, 0, 0, 0, false, 0, 0, 0, false, false, hand.IsLeftHand, false, false, false));
                    }
                }
            }
        }

        [HarmonyPatch(typeof(Il2CppVertigo.AZS2.Client.ShootStrategy), "StopShooting")]
        public class StopShootingPatch
        {
            [HarmonyPostfix]
            public static void Postfix(Il2CppVertigo.AZS2.Client.ShootStrategy __instance)
            {
                try
                {
                    MelonLogger.Msg("Hooked on stop shooting");

                    if (_shootingID == null)
                    {
                        MelonLogger.Error("_shootingID is null");
                        return;
                    }

                    if (_skineticHandler == null)
                    {
                        MelonLogger.Error("_skineticHandler is null");
                        return;
                    }

                    foreach (int id in _shootingID)
                    {
                        MelonLogger.Msg($"Stopping effect {id}");
                        _skineticHandler.Stop(id, 0.05f);
                    }
                    _shootingID.Clear();
                }
                catch (Exception ex)
                {
                    MelonLogger.Error($"Exception in StopShootingPatch.Postfix: {ex.Message}\n{ex.StackTrace}");
                }

            }
        }

        #endregion

        #region movements
        [HarmonyPatch(typeof(ClientPlayerHealthModule))]
        [HarmonyPatch("add_OnDamagedEvent")]
        public class ClientPlayerHealthModulePatch
        {
            [HarmonyPostfix]
            public static void Postfix(ClientPlayerHealthModule __instance)
            {
                _playerPosition = __instance.transformModule.HeadPosition;
                _playerRotation = __instance.transformModule.ChestRotation;
                _posInstance = __instance;
            }
        }
        #endregion

        #region environement
        [HarmonyPatch(typeof(ClientExplosiveItemFeature), "Explode")]
        public class Explode
        {

            public static float CalculateForce(float distance)
            {
                float force;
                if (distance < 6)
                {
                    force = 100f;
                    _skineticHandler.Play(Patterns.Explosion, SkineticManager.InitEffectProperties(3, force));
                }
                else if (distance > 23)
                {
                    force = 30f;
                    _skineticHandler.Play(Patterns.Explosion, SkineticManager.InitEffectProperties(10, force));
                }
                else
                {
                    float forceMin = 30f;
                    float forceMax = 100f;
                    float distanceMin = 6f;
                    float distanceMax = 23f;

                    force = forceMax - ((distance - distanceMin) / (distanceMax - distanceMin) * (forceMax - forceMin));
                    _skineticHandler.Play(Patterns.Explosion, SkineticManager.InitEffectProperties(5, force));
                }

                return force;
            }

            [HarmonyPostfix]
            public static void Postfix(ClientExplosiveItemFeature __instance)
            {
                float distance = Vector3.Distance(_playerPosition, __instance.clientItemBehaviour.gameObject.transform.position);
                float force = CalculateForce(distance);

            }
        }

        [HarmonyPatch(typeof(UIButtonHapticsBehaviour), "PlayHapticsOnActiveHand")]
        public class UIHaptics
        {
            [HarmonyPostfix]
            public static void Postfix(UIButtonHapticsBehaviour __instance)
            {
                _skineticHandler.Play(Patterns.UI, SkineticManager.InitEffectProperties(9, 70));
            }
        }
        #endregion
    }
}
